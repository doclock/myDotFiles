"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

colorscheme onedark
set number
let mapleader=" "
" enter the current millenium
set nocompatible
set hidden
set list
set listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨
" enable syntax and plugins (for netrw)
"syntax on
syntax enable

" INDENTING:
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
set autoindent
set softtabstop=4
set scrolloff=8
" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu


" FILE BROWSING:
" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
"let g:netrw_browse_split=4  " open in prior window (above)
"let g:netrw_browse_split=3  " open in filetree
let g:netrw_browse_split=2  " open in split to right
"let g:netrw_browse_split=1  " open in split above
"let g:netrw_browse_split=0  " open in new window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
" NOW WE CAN:
" - :edit a folder to open a file browser
" - <CR>/v/t to open in an h-split/v-split/tab
" - check |netrw-browse-maps| for more mappings


" SNIPPETS:
" Read an empty HTML template and move cursor to title
nnoremap ,html :-1read $HOME/.vim/.skeleton.html<CR>3jwf>a


"nnoremap <C-d> <C-d>zz
"nnoremap <C-u> <C-u>zz 
" CTAGS:
" Create the `tags` file (may need to install ctags first)
command! MakeTags !ctags -R .
" ctags generation
nnoremap <leader>c :!ctags -R .<cr><cr>
" NOW WE CAN:
" - Use ^] to jump to tag under cursor
" - Use g^] for ambiguous tags
" - Use ^t to jump back up the tag stack
" WE CAN ALSO AUTOCOMPLETE NOW WITH:
" - ^x^n for JUST this file
" - ^x^f for filenames (works with our path trick!)
" - ^x^] for tags only
" - ^n for anything specified by the 'complete' option
 
" KEYMAPS:
" searching ang grepping
"nnoremap <leader>f :E<cr>
nnoremap <leader>sf :find<SPACE>
nnoremap <leader>g :grep<SPACE>
nnoremap <leader>cc :w !gcc % -o %.o<cr>
nnoremap <leader>rc :!./%.o<cr>
nnoremap <leader>rp :w !python3 %<cr>
nnoremap <leader>rw ;!xdg-open %<cr><cr>
"nnoremap <leader>x :'<,'> s/^/#<cr>
"nnoremap <leader>x :edit .<cr>
"noremap <leader>h :vertical resize 50%<cr>
"
"nnoremap <leader>a :vertical resize 200<cr>
"nnoremap <leader>s :vertical resize 25<cr>
"nnoremap <leader>= :vertical resize +10<cr>
"nnoremap <leader>- :vertical resize -10<cr>
"nnoremap K :Ggrep \b<C-R><C-W>\b"<cr>:cw<cr>
noremap <silent> <C-S-Left> :vertical resize +10<CR>
"noremap <silent> <C-S-&gt> :vertical resize +10<CR>
noremap <silent> <C-S-Right> :vertical resize -10<CR>
"noremap <silent> <C-S-&lt> :vertical resize -10<CR>


" Use ctrl-[hjkl] to select the active split!
nnoremap <leader>gh <C-W><C-H>
nnoremap <leader>gj <C-W><C-J>
nnoremap <leader>gk <C-W><C-K>
nnoremap <leader>gl <C-W><C-L>

"edit .<cr>

" CURSOR_HACKS:
" Skinny cursor on INSERT
let &t_SI = "\<Esc>]40;CursorShape=1\x7" 
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"
" Exit Lag Fix
set ttimeout
set ttimeoutlen=1
set ttyfast
 
" Insert char at cursor position
" nnoremap <C-i> i <ESC>r
" nnoremap I i <ESC>r
" nnoremap <space>i i <ESC>r
" nnoremap m i <ESC>r
" Append char after cursor position
" nnoremap <C-a> a <ESC>r
" nnoremap A a <ESC>r
" nnoremap A a <ESC>r
nnoremap s :exec "normal i".nr2char(getchar())."\e"<CR>
nnoremap S :exec "normal a".nr2char(getchar())."\e"<CR>


