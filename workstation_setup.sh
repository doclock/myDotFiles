# Install Utilities and Dependencies
sudo apt-get install -y tmux xclip xsel  ninja-build gettext cmake unzip curl gcc git pkg-config nasm libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3 ffmpeg cargo
sudo apt install -y maven openjdk-17-jdk openjdk-17-doc openjdk-17-dbg python3-pip terminator vim python3.10-venv jupyter npm

# Set up git
git config --global user.email "michael.c.locker@gmail.com"
git config --global user.name "doclock"

# Create Applications Directory
mkdir ~/Applications
cd ~/Applications

# Set Neovim
git clone https://github.com/neovim/neovim
cd neovim/ && make CMAKE_BUILD_TYPE=RelWithDebInfo && sudo make install && cd ..

# Set Alacritty
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh &&
git clone https://github.com/jwilm/alacritty.git
cd alacritty/ && cargo build --release && sudo cp target/release/alacritty /usr/local/bin && sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg && sudo desktop-file-install extra/linux/Alacritty.desktop && echo "source $(pwd)/extra/completions/alacritty.bash" >> ~/.bashrc &&
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/alacritty 50 &&
gsettings set org.cinnamon.desktop.default-applications.terminal exec /usr/local/bin/alacritty &&
gsettings set org.gnome.desktop.default-applications.terminal exec-arg "-x" &&
sudo update-alternatives --config x-terminal-emulator

# Add the rest of my software sweet
sudo apt install -y rpi-imager putty cura blender kicad openscad obs-studio

# Clone my dot files
git clone http://www.gitlab.com/doclock/myDotFiles.git

sudo apt install jupyter
pip install jupyter-lab
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc

sudo pip3 install radio-active --upgrade
